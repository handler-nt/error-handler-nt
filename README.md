# ErrorHandler

Module that provides functions for error handling.

## Installation
<code>$ npm install error-handler-nt --save</code>

## Documentation

- <code>create(status = 500, message = "", origin = "")</code><br>
	
	Returns CError object

- <code>cast(err, origin = "")</code> <br>
	
	Cast error to CError<br>
	
- <code>send(res, err)</code><br>

	Send the error as a response (only for Express framework)
	
- <code>createAndSend(res, status = 500, message = "", origin = "")</code><br>

	Create and send an error in the same function (only for Express framework)
	
- <code>log(err)</code><br>

	Log the error