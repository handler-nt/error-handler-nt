const assert = require('assert');
const lib = require('../index.js');


describe("Create Error", () =>
{
  it("Default error", () =>
  {
    let err500 = lib.create();

    assert.equal(err500.status, 500);
  });

  it("Custom error", () =>
  {
    const err = lib.create(401, {message: "Unauthorized", type: "UNAUTHORIZED"}, "Testing function");
    assert.equal(err.type, "UNAUTHORIZED");
    assert.equal(err.status, 401);
  })
});

describe("Cast error", () =>
{
  it("Simple casting", () =>
  {
    const err = new Error("test");
    err.status = 200;

    const castedErr = lib.cast(err, "testing...");

    assert.equal(castedErr.message, err.message);
    assert.equal(castedErr.status, 200);
    assert.equal(castedErr.origin, "testing...");
  });

  it("CError to CError", () =>
  {
    const cErr = lib.create(200, "OK");
    const castedErr = lib.cast(cErr);

    assert.equal(castedErr.status, 200);
  });

});


describe('Logging error', () =>
{

  it("Simple Error logging", () =>
  {
    const err = new Error("Forbidden");
    err.status = 400;

    lib.log(err);

  });

  it("CError logging", () =>
  {
    const cErr = lib.create(400, {message: "Forbidden", type: "FORBIDDEN"}, "Test");
    lib.log(cErr);
  });

});





