// by NikosiTech

const CError = require('./lib/CError');
const logHandler = require('log-handler-nt');


/**
 * Create an CError object
 * @param {number} status
 * @param {string|object} message
 * @param {string=} origin
 */
function create(status = 500, message = "", origin = "")
{
  return new CError(status, message, origin);
}


/**
 * Cast error to CError
 * @param {Error} err
 * @param {string} origin
 * @return {*}
 */
function cast(err, origin = "")
{
  if (err.constructor.name === "CError")
    return err;

  const status = (typeof err.status === 'undefined' ? 500 : err.status);
  const message = (typeof err.message === 'undefined' ? "": err.message);
  const newErr = new CError(status, message, origin);
  newErr.stack = err.stack;

  return newErr;
}


/**
 * Send the error as response
 * @param {*} res
 * @param {Error} err
 * @return {*}
 */
function send(res, err)
{
  if (res === null)
    return;

  if (err === null)
    return res.sendStatus(500);


  if (!err.hasOwnProperty("status") || typeof err.status === 'undefined' || err.status === null || err.status > 500)
    err['status'] = 500;

  log(err);

  if (err.status === 500)
    return res.sendStatus(500);


  const retObj = {message: err.message};
  if (err.hasOwnProperty("type"))
    retObj['type'] = err.type;

  return res.status(err.status).json(retObj);
}


/**
 * Create and Send an error in the same function
 * @param {object} res
 * @param {number} status
 * @param {string|object} message
 * @param {string} origin
 */
function createAndSend(res, status = 500, message = "", origin = "")
{
  const err = create(status, message, origin);
  return send(res, err);
}


/**
 * Log Error
 * @param {Error} err
 */
function log(err)
{
  if (err.constructor.name === "CError")
    logHandler.log('error', err.display());
  else
    logHandler.log('error', err.message);
}

module.exports =
{
  send,
  create,
  cast,
  log,
  createAndSend
};