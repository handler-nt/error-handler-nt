// by NikosiTech

class CError extends Error
{
  /**
   * @constructor CError
   * @param {number} status
   * @param {string | Object} message
   * @param {string} origin
   * @param {Object} req
   */
  constructor(status, message = "", origin = "", req = null)
  {
    if (typeof message === 'string')
      super(message);
    else
    {
      if (message.hasOwnProperty("message") || message.hasOwnProperty("type"))
      {
        super(message.message);
        this.type = message.type;
      }
      else
        super("");
    }

    this.status = status;
    this.origin = origin;
    this.req = req;
  }

  display()
  {

    let retStr = "";

    if (this.origin !== "")
      retStr += this.origin+" : ";

    retStr += this.status+" : "+this.message;

    if (this.hasOwnProperty("type"))
      retStr += " - "+this.type;

    return retStr;

  }

}


module.exports = CError;
